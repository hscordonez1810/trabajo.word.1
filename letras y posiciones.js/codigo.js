var letras = [];

function agregarLetraInicio(letra) {
    let posicion = letras.indexOf(letra);
    if (posicion == -1) {
        letras.unshift(letra);
        imprimir1();
    } else {
        alert('Ya existe la letra');
    }
}

function agregarLetraFin(letra) {
    let posicion = letras.indexOf(letra);
    if (posicion == -1) {
        letras.push(letra);
        imprimir1();
    } else {
        alert('Ya existe la letra');
    }
}

function eliminarLetraInicio() {
    if (letras.length == 0) {
        alert('No existe elementos para eliminar');
    } else {
        letras.shift();
        imprimir1();
    }

}


function eliminarLetraFin() {
    if (letras.length == 0) {
        alert('No existe elementos para eliminar');
    } else {
        letras.pop();
        imprimir1();
    }
}

function eliminarLetra(letra) {
    let posicion = letras.indexOf(letra);
    if (posicion > -1) {
        letras.splice(posicion, 1);
        imprimir1();
    } else {
        alert('Letra no encontrada');
    }
}
// a b c
function imprimir1() {
    document.getElementById('body1').innerHTML = '';
    for (let i = 0; i < letras.length; i++) {
        document.getElementById('body1').innerHTML +=
            `<tr>
            <td>
                ${letras[i]}
            </td>
            </tr>
            `;
    }
}

function imprimir2() {
    document.getElementById('body2').innerHTML = '';
    for (let i = 0; i < letras.length; i++) {
        document.getElementById('body2').innerHTML +=
            `<tr><td>${letras[i]}</td></tr>`;
    }
}